import React from 'react'

window.$ = window.jQuery = require('jquery')
window.MotionUI = require('motion-ui')

require('foundation-sites')

$(function(){
    $(document).foundation()
})
