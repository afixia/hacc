# Setup #
```
git clone git@bitbucket.org:afixia/hacc.git
cd hacc
npm install -g gulp gulp-cli webpack
npm install
```

## Sass ##
src/sass/app.scss

## React ##
src/js/app.js

* es6 support from webpack / babel loader
```
import React from 'react'
import {YourComponent} from './components/MyComponent.js'
```

# Development Server && Runtime Reload #
defaults to port 8000, can be changed in:
* server.js on line 9

to run the development and runtime compile on change, simply run this command from the root directory

```
gulp
```

the local development environment document root is in
* public/

## Dist Site Structure ##

* public/images/
* public/index.html
* public/css/app.css
* public/js/app.js
